#pragma once
#include "cuda_runtime.h"

#ifndef CONTRACTION_CUH
#define CONTRACTION_CUH

/*
----------------------------------------------------------------------
Contraction as matrix-matrix product
----------------------------------------------------------------------
*/

void gmemm_(
    
);

__host__ void contract_dtensors_gemm(dTensor* A, dTensor* B, dTensor* C, int r);

#endif