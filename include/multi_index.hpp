#ifndef MULTI_IND_H
#define MULTI_IND_H

class MultiIndex {
  /* 
   ------------------------------------------------------------------------------
      MultiIndex --- Data structure for iterating over the elements of a dTensor
   ------------------------------------------------------------------------------
   */
  public:
    int dim;      // Number of dimensions for the index
    int* shape;   // # of indices per dimension
    int* curr;    // Current value (i_1, ..., i_dim) of this multi-index  
   
  public:
    MultiIndex(int dim, int* shape);
    ~MultiIndex();
    void zero();
    int step();
    int& operator[](int i);
};

#endif
