#pragma once
#include "cuda_runtime.h"

#ifndef HENON_HEILES_CUH
#define HENON_HEILES_CUH

#include "dtensor.cuh"

__global__ void henon_heiles_contract_right_kernel(
    dTensor* X,         // Input tensor
    dTensor* Y,         // Output tensor
    int n_a,            // Number of degrees of freedom for mode-1 of X
                        //    Should match X->shape[0]
    int n_dof,          // Number of physical degrees of freedom,
                        //    should match X->shape[2]
    float lambda,       // Scaling coefficient for the potential
    float q_min,        // min/max q-values for the site 
    float q_max,
    int N_WpBlk
);

void henon_heiles_contract_right(
    dTensor* X, 
    dTensor* Y, 
    float lambda, 
    float q_min, 
    float q_max,
    int N_WpBlk
);

void henon_heiles_contract_right(
    dTensor* X, 
    dTensor* Y, 
    float lambda, 
    float q_min, 
    float q_max
);

#endif