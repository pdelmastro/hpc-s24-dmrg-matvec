#pragma once
#include "cuda_runtime.h"

#ifndef TENSOR_H
#define TENSOR_H

#include <string>
#include "multi_index.hpp"
#include <my_cuda_util.cuh>


class dTensor {
/*
 -------------------------------------------------------------------------------
  dTensor --- a dense double tensor
 -------------------------------------------------------------------------------
 */

  /*
  -------------------------------------------------------------------------------
   Class Fields
  -------------------------------------------------------------------------------
  */
  public:
    int dim;            // Tensor order (# of dimensions)
    int* shape;         // # of indices per dimension
    int numel;          // Total number of elements in the tensor
    bool on_device;     // Flag. Set to True to have this dtensor be stored
                        //    on the device
    double* t;          // Linear array storing the elements of the tensor
                        // ** Elmts are stored in COLUMN MAJOR order
    int* idx;           // Extra arrays used within the array for indexing 
    // DEVICE copy of the dTensor, initialized only if on_device=True
    dTensor* dT;
    // Extra HOST variable, initialized on host only if on_device=True
    int* h_shape;

  /*
  -------------------------------------------------------------------------------
    Basic dTensor Operations 
  -------------------------------------------------------------------------------
  */

  public:
    __host__ dTensor();
    __host__ dTensor(int dim, int d0, ...);
    __host__ dTensor(int dim, int* shape);
    __host__ ~dTensor();

    __host__ static dTensor* new_device_dtensor(int dim, int* shape);
    __host__ static void free_device_dtensor(dTensor* T);

    __host__ void reshape_fixed_dim(int* new_shape);
  
    __host__ int flatten_idx(int* idx);
    __host__ double& operator() (int* idx);
    __host__ double& operator() (int i0, ...);
    __host__ double& operator() (MultiIndex* midx);
    
    __host__ void zero();
    //void urand();
    __host__ void urand(curand_state* states);
  /*
  -------------------------------------------------------------------------------
    Tensor contractions
  -------------------------------------------------------------------------------
  */
  
  public:

    __host__ static dTensor* contract(dTensor* A, dTensor* B, std::string A_fmt,
                                        std::string B_fmt, std::string C_fmt);
    
    __host__ static double inner_product(dTensor* A, dTensor* B); 

    __host__ static void device_contract_final_idx(
                    dTensor* A, dTensor* B, dTensor* C,
                    int n_shared_dims, int permute, int* pi_A, int* pi_B
                );

};


#endif