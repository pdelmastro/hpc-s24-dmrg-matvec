#ifndef CONTRACTOR_H
#define CONTRACTOR_H

#include <dtensor.cuh>

using namespace std;
#include <string>

class Contractor {
    /* 
    ------------------------------------------------------------------------------
      Contractor --- Structure to create and store meta-data needed for 
                      tensor contractions
    ------------------------------------------------------------------------------
    */
    // Internal variables used to compute the contraction
    public:
        dTensor* A;                     // Tensors to be contracted
        dTensor* B;

        int n_shared_dims;              // Number of dimensions shared btwn A and B
        int* shared_shape;              // Shape of the shared index space
        
        int n_As_unshared_dims;         // Number of unshared dimensions for A 
        int* As_shared_dims;            // Indices of the shared dimensions of A
        int* As_unshared_dims;          // Indices for the unshared dimensions of A
        int* As_unshared_dims_perm;     // Mapping from the unshared dimensions of A to
                                        //  the indices of the contracted tensor C.
                                        //  In particular, As_unshared_dims_perm[i] is
                                        //  the index of C associated with As_unshared_dims[i]
        
        int n_Bs_unshared_dims;         //
        int* Bs_shared_dims;            // Same as the variables above, just for the
        int* Bs_unshared_dims;          // tensor B
        int* Bs_unshared_dims_perm;     //

        int new_dim;                    // Number of dimensions in the contracted tensor
        int* new_shape;                 // Shape of the new tensor

    
    // Constructor and destructor
    public:
        Contractor(dTensor* A, dTensor* B, string A_fmt, string B_fmt, string C_fmt);
        ~Contractor();

    // Main contraction functions
    public:
        dTensor* contract();

    // Contraction helper function
    private:
        void parse_contraction_pattern(string A_fmt, string B_fmt, string C_fmt);
};

#endif