#ifndef UTIL_H
#define UTIL_H

using namespace std;
#include <string>

float urandf();
int sign(double d);
int int_min(int a, int b);
int int_array_contains(int* arr, int length, int query);
int str_array_contains(string arr, char query);

template <class T> T array_product(int n, T* arr) {
    /* Returns the product of the elements the array */
    if (n == 1) 
        return arr[0];
    else
        return arr[0] * array_product(n-1,arr+1);
}

template <class T> T array_mean(int n, T* arr) {
    /* Returns the mean value of the array */
    T acc = 0;
    for (int i = 0; i < n; i++) {
        acc += arr[i];
    }
    return acc / n;
}



#endif