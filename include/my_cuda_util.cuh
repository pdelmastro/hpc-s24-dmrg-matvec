#pragma once
#include <cuda_runtime.h>

#ifndef CUDA_UTIL_H
#define CUDA_UTIL_H

#include <stdio.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>


/* 
-----------------------------------------------------------------------
    Basic Device Manipulation
-----------------------------------------------------------------------
*/
__host__ void set_device(int device_num);

void* new_device_variable(size_t size);
void set_device_variable(void* d_ptr, void* h_ptr, size_t size);
void* get_device_variable(void* ptr, size_t size);
void free_device_variable(void* v);

void* new_device_array(int N, size_t size);
void copy_array_to_device(int N, size_t size, void* h_arr, void* d_arr);
void copy_array_from_device(int N, size_t size, void* d_arr, void* h_arr);
void free_device_array(void* d_arr);
void zero_device_array(int N, size_t size, void* d_arr);


/* 
-----------------------------------------------------------------------
    Random numbers
-----------------------------------------------------------------------
*/
// RNG type options: curandState, curandStateMRG32k3a_t, curandStatePhilox4_32_10_t
typedef curandStateMRG32k3a_t curand_state;
// typedef curandState curand_state;

curand_state* new_curand_states(int N, int seed);
__global__ void new_curand_states_kernel(int N, curand_state* state, int seed);

void urand_device_array(int N, double* d_arr, curand_state* state);
__global__ void urand_device_array_kernel(int N, double* d_arr, curand_state *state);


/* 
-----------------------------------------------------------------------
    Misc
-----------------------------------------------------------------------
*/
__device__ int device_int_array_product(int N, int* d_arr);


#endif
