CPP_SRC_DIR=src/cpp
CU_SRC_DIR=src/cuda
INC_DIR=include
TEST_DIR=test
CPP_OBJ_DIR=obj/cpp
CU_OBJ_DIR=obj/cuda

CC = g++
NVCC = nvcc
CFLAGS = -c -I $(INC_DIR)/ -I /usr/local/cuda/include
CU_FLAGS = -arch=sm_61 -G 
LINK_OPTIONS = -lm -lcudart

OBJECTS = \
	$(CPP_OBJECTS) \
	$(CU_OBJECTS)

CPP_OBJECTS =   \
	$(CPP_OBJ_DIR)/util.o 	\
	$(CPP_OBJ_DIR)/contractor.o  \
	$(CPP_OBJ_DIR)/multi_index.o  \

CU_OBJECTS =   \
	$(CU_OBJ_DIR)/dtensor.o 	\
	$(CU_OBJ_DIR)/my_cuda_util.o  \
	$(CU_OBJ_DIR)/henon_heiles.o  \
	$(CU_OBJ_DIR)/contraction.o  \

TEST_OBJECTS = \
	$(TEST_DIR)/mpo_matvec_step2.o \
	$(TEST_DIR)/mpo_matvec_step3.o \
	$(TEST_DIR)/mpo_matvec.o \
	$(TEST_DIR)/dtensor_test.o \
	$(TEST_DIR)/contractor_test.o \
	$(TEST_DIR)/mpo_matvec.o \

TESTS = \
	$(TEST_DIR)/mpo_matvec \
	$(TEST_DIR)/contractor_test \
	$(TEST_DIR)/dtensor_test \
	

all: \
	obj_dir \
	$(OBJECTS) \
	$(TESTS) \

# Object directory
obj_dir:
	mkdir -p obj
	mkdir -p obj/cpp
	mkdir -p obj/cuda

# CPP files from src
$(CPP_OBJ_DIR)/%.o: $(CPP_SRC_DIR)/%.cpp obj_dir
	$(CC) $(CFLAGS) -o $@ $<

# CUDA files from src
$(CU_OBJ_DIR)/%.o: $(CU_SRC_DIR)/%.cu obj_dir
	$(NVCC) $(CFLAGS) $(CU_FLAGS) -dc -o $@ $<

# CPP files from test
$(TEST_DIR)/%.o: $(TEST_DIR)/%.cpp 
	$(CC) $(CFLAGS) -o $@ $<

# CUDA files from test
$(TEST_DIR)/%.o: $(TEST_DIR)/%.cu
	$(NVCC) $(CFLAGS) $(CU_FLAGS) -dc -o $@ $<

# EXE from test
$(TEST_DIR)/%: $(TEST_DIR)/%.o $(OBJECTS)
	$(NVCC) $(CU_FLAGS) -dlink $(CU_OBJECTS) $@.o -o $@_link.o
	$(CC) $@.o $(OBJECTS) $@_link.o $(LINK_OPTIONS) -o $@.exe
	rm $@_link.o

# CPP files from run
$(RUN_DIR)/%.o: $(RUN_DIR)/%.cpp 
	$(CC) $(CFLAGS) -o $@ $<

# EXE from run
$(RUN_DIR)/%: $(RUN_DIR)/%.o $(OBJECTS)
	$(CC) $@.o $(OBJECTS) -o $@.exe

clean:
	rm $(CPP_OBJ_DIR)/*.o
	rm $(CU_OBJ_DIR)/*.o
	rm $(TEST_DIR)/*.o
	rm $(TEST_DIR)/*.exe
	rm $(RUN_DIR)/*.exe
