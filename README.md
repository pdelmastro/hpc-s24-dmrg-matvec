# DMRG "Matvec" Contraction Sequence

Peter DelMastro, Math 5414, Spring 2024

---

Basic C++/CUDA implementation of a tensor contraction to be used within the density matrix renormalization (DMRG) algorithm. Refer to the report [here](https://www.overleaf.com/read/rrthswjwdbhk#b7da5b) for brief overview of tensor trains & DMRG, discussion of considerations for implementing tensor contractions on the GPU, and details of the tensor contraction implemented in code here.

## Some Code of Interest

`src/cuda/dtensor.cu`: Basic implementation of (double) tensors of arbitrary dimension and shape as a C++ class. Most functionality, such as indexing using the `()` operator, can only run on the CPU, but the class provides a routine for creating tensors on the device, nested within a host `dTensor` object.

`src/cuda/henon_heiles.cu`: Implementation of the tensor train operator contraction, described in Section 1.3.1 of my [report](https://www.overleaf.com/read/rrthswjwdbhk#b7da5b). This operator in particular is based on a generalization of the Henon-Heiles potential to higher-dimensions (c.f. Eqn 4.8 of [this paper](https://www.semanticscholar.org/paper/DMRG%2BQTT-approach-to-computation-of-the-ground-for-Khoromskij-Oseledets/bc917efaf4f7b1f65c924f4dd03bd2f9c90a5eeb)).  My kernel performs a "matrix-free" linear operation on four-dimensional `dTensor` objects.

`test/mpo_matvec.cu`: This program runs the Henon Heiles contraction kernel multiple times for the sake of profiling the kernel. The script requires four command line arguments as follows
```
./mpo_matvec.exe PHYS_DIM BOND_DIM N_WpBLK N_TRIALS
```
The second parameter `BOND_DIM` should be a multiple of 32 to ensure full warp utilization. The report uses `BOND_DIM=32, N_WpBLK=16, N_TRIALS=50` and `PHYS_DIM` in `{64, 128, 256, 384}`. Be a bit careful here, as the size of the tensors grow as the *square* of `PHYS_DIM * BOND_DIM`, so they get large quickly. I ran out of memory for `PHYS_DIM=512`. 

`src/cpp/contractor.cpp`: Class for performing dense tensor contractions *on the host* using Einstein notation. For If `A` and `B` are pointers to tensors of shape `(4,2,3)` and `(3,2,8)`,
```
dTensor::contract(A, B, "ijk", "jkl", "li");
```
will return a pointer to new tensor `C` of shape `(8,4)` with elements
$$
C(l,i) = \sum_{j,k} A(i,j,k) B(j,k,l)
$$

`src/cpp/multi_index/cpp` provides a class that aids in the tensor indexing needed for the `Contractor` class.

`src/cuda/contraction.cu`: Functions to perform dense tensor contractions on the GPU, albeit the function using a matrix-matrix product is not finished. This implementation was to use a GEMM function from another [repo](https://github.com/siboehm/SGEMM_CUDA/blob/master/src/kernels/10_kernel_warptiling.cuh), based on [this blog post](https://siboehm.com/articles/22/CUDA-MMM).

`test/dtensor_test.cpp` and `test/contractor_test.cpp` are just basic tests of the host functionality of the `dTensor` class. 


