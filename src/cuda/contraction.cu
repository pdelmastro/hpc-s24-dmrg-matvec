#include <stdlib.h>
#include <unistd.h>
#include <cuda.h>
#include "dtensor.cuh"
#include "contraction.cuh"
#include "my_cuda_util.cuh"
#include "util.hpp"
#include "sgemm.cuh"

#define W 32 // Warp size 

/*
--------------------------------------------------------------------------
    Contraction based on matrix-matrix product
--------------------------------------------------------------------------
*/


__host__ void contract_dtensors_gemm(dTensor* A, dTensor* B, dTensor* C, int r) {
    /*
        Contracts tensors A and B along the last r modes of A and the first
        r modes of B.

        Contraction performed as a matrix-matrix product

        Result of the contraction is stored in the tensor C
     */
    
    /* Dimensions of the matricized tensors A and B */
    /* A will be M x K matrix and B will be K x N matrix */
    int M = array_product<int>(A->dim-r, A->h_shape);
    int K = A->numel / M;
    int N = B->numel / K;

    /* Contraction via matrix-matrix product */
    sgemm(
        M, N, K, 1.0, (float*)A, (float*) B, 0.0, (float*) C
    );

}

