#include <malloc.h>
#include <stdarg.h>
#include <iostream>
#include <cuda.h>
#include "dtensor.cuh"
#include "contractor.hpp"
#include "multi_index.hpp"
#include "util.hpp"
#include "my_cuda_util.cuh"
#include "contraction.cuh"

/*
 -------------------------------------------------------------------------------
  dTensor constructors + destructor
 -------------------------------------------------------------------------------
 */

__host__ dTensor::dTensor() {}

__host__ dTensor::dTensor(int dim, int d0, ...) {
    /* 
        Creates a 'dim'-dimensional tensors of the given shape,
        specified as a sequence of integer arguments d0, ...
     */
    va_list args;
    va_start(args, d0);
    int* shape = (int*)malloc(sizeof(int)*dim);
    // Unpacking the variadic arguments
    for (int i = 0; i < dim; i ++) {
        shape[i] = (i == 0) ? d0 : va_arg(args, int);
    }
    va_end(args);
    // Calling the other contructor
    new (this) dTensor(dim, shape);
}

__host__ dTensor::dTensor(int dim, int* shape) {
    /*
        Creates a 'dim'-dimensional tensors of the given shape,
        specified as an integer array
    */
    this->dim = dim;
    this->shape = shape;
    this->numel = array_product<int>(dim, shape);
    this->on_device = false;
    
    // Allocating space for the data
    this->t = (double*)malloc(sizeof(double)*numel);

    // Internal arrays used for tensor indexing
    this->idx = (int*)malloc(sizeof(int)*dim);
}


__host__ dTensor::~dTensor() {
    /* Frees the space allocated for this tensor */

    //
    if (this->on_device) {
        free_device_array(this->shape);
        free_device_array(this->idx);
        free_device_array(this->t);
        free_device_variable(dT);
        free(this->h_shape); // Extra copy of the shape on the host
    } 
    else {
        free(this->shape);
        free(this->idx);
        free(this->t);
    }
}


__host__ dTensor* dTensor::new_device_dtensor(int dim, int* shape) {
    /* 
        Creates a 'dim'-dimensional tensors of the given shape,
        specified as a sequence of integer arguments d0, ...

        The dTensor will be stored on the device
     */
    // Host dTensor object, to contain the device dTensor
    // pointers and a pointer to the dTensor that's actually on the 
    // device
    dTensor* hT = (dTensor*)malloc(sizeof(dTensor));

    // HOST VARIABLES
    hT->dim = dim;
    hT->h_shape = shape;
    hT->numel = array_product<int>(dim, shape);
    hT->on_device = true;

    /*
        SETTING UP THE DEVICE COPY OF THE TENSOR NOW
    */
    dTensor* dT = (dTensor*)new_device_variable(sizeof(dTensor)); 
    hT->dT = dT;

    // Basic variables
    set_device_variable(&(dT->dim), &dim, sizeof(int));
    set_device_variable(&(dT->numel), &(hT->numel), sizeof(int));
    
    // Shape of the tensor
    hT->shape = (int*)new_device_array(dim, sizeof(int)); 
    copy_array_to_device(dim, sizeof(int), shape, hT->shape);
    set_device_variable(&(dT->shape), &(hT->shape), sizeof(int*));
    
    // Indexing array
    hT->idx = (int*)new_device_array(dim, sizeof(int));
    set_device_variable(&(dT->idx), &(hT->idx), sizeof(int*));
    
    // Finally, space for the data 
    hT->t = (double*)new_device_array(hT->numel, sizeof(double));
    set_device_variable(&(dT->t), &(hT->t), sizeof(double*));

    return hT;
}


__host__ void dTensor::reshape_fixed_dim(int* new_shape) {
    /* Reshapes the tensor, 
       Assumes the number of dimensions does not change 
    */
    if (this->on_device) {
        // Host copy of shape
        this->h_shape = new_shape;
        // Copy to the device
        copy_array_to_device(this->dim, sizeof(int), this->h_shape, this->shape);
    }    
} 



/*
 -------------------------------------------------------------------------------
  dTensor element access
 -------------------------------------------------------------------------------
 */

//TODO  CONVERT TO RECURSIVE FUNCTION???
__host__ int dTensor::flatten_idx(int* idx) {
    /* Converts a tensor idx = (i_0, ..., i_n) to the index 
       of the associated index in the flattened 1D array 
       
       Uses COLUMN MAJOR ORDERING
     */
    int j = 0;
    for (int i = this->dim - 1; i > 0; i--) {
        j = this->shape[i-1] * (j + idx[i]);
    }
    j += idx[0];
    // printf("i0=%d, i1=%d, i2=%d, i3=%d, j=%d\n", idx[0], idx[1], idx[2], idx[3], j);
    return j;
}


__host__ double& dTensor::operator()(int i0, ...) {
    /* Gets the element at index idx = [i_0, i_1, i_2]
       
       WARNING: Does not check for out of bounds errors 
     */
    
    // Loading the arguments into an array
    va_list args;
    va_start(args, i0);
    this->idx[0] = i0;
    for (int i = 1; i < this->dim; i++) {
        this->idx[i] = va_arg(args, int);
    }
    va_end(args);
    // Getting the item now
    return (*this)(this->idx);
}


__host__ double& dTensor::operator()(int* idx) {
    /* Gets the element at index idx = [i_0, ... i_{dim-1}]

       Overload of the previous method to allow for 
       indexing using int arrays
       
       WARNING: Does not check for out of bounds errors 
     */
    int i = this->flatten_idx(idx);
    return this->t[i];
}


__host__ double& dTensor::operator()(MultiIndex* midx) {
    /* Gets the element at index idx = [i_0, i_1, i_2]

       Overload of the previous method to allow for 
       indexing using MultiIndex objects
       
       WARNING: Does not check for out of bounds errors 
     */
    int i = this->flatten_idx(midx->curr);
    return this->t[i];
}


/*
 -------------------------------------------------------------------------------
  dTensor data filling
 -------------------------------------------------------------------------------
 */


__host__ void dTensor::zero() {
    /* 
    Sets all of this tensor's elements to zero
    */
    if (this->on_device) {
        zero_device_array(this->numel, sizeof(double), this->dT->t);
    } else {
        for (int i = 0; i < this->numel; i++)
            this->t[i] = 0.0;
    }
}


__host__ void dTensor::urand(curand_state* states) {
    /*
     Randomizes the values in this tensor, with each element
     being drawn iid from U[-1,1]
     */
    if (this->on_device) {
        urand_device_array(this->numel, this->t, states);
    }
}


/*
 -------------------------------------------------------------------------------
  Tensor contraction (host) 
 -------------------------------------------------------------------------------
 */

__host__ dTensor* dTensor::contract(
        dTensor* A, dTensor* B, 
        std::string A_fmt, std::string B_fmt, std::string C_fmt
    ) {
    /*
        Contracts tensors A and B using the specified contraction pattern, 
        written in Einstein summation notation.
    */
    /* Contraction pattern parsing, using a Contraction obj */
    Contractor* contractor = new Contractor(A, B, A_fmt, B_fmt, C_fmt);
    /* Performing the contractions */
    dTensor* C = contractor->contract();
    /* Clean up + return */
    delete contractor;
    return C;
}



__host__ double dTensor::inner_product(dTensor* A, dTensor* B) {
    /* 
        Contracts two tensors of the size shape to a single scalar 
        value by contracting over their physical indices

        Requires that A->shape matches B->shape 
     */
    // Verify the tensor shapes
    if (A->dim != B->dim) {
        throw runtime_error("dTensor Inner Product"
                            "--- Tensor dimensions don't match\n");
    }
    for (int i = 0; i < A->dim; i++) {
        if (A->shape[i] != B->shape[i]) {
            throw runtime_error("dTensor Inner Product"
                            "--- Tensor shapes don't match\n");
        }
    }
    // Computing the inner product
    MultiIndex* idx = new MultiIndex(A->dim, A->shape);
    double acc = 0.0;
    for (int s = 0; s < A->numel; A++) {
        acc += (*A)(idx) * (*B)(idx); 
        idx->step();
    }
    delete idx;
    return acc;
}



/*
 -------------------------------------------------------------------------------
  Tensor contractions --- host calling device
 -------------------------------------------------------------------------------
 */

__host__ void dTensor::device_contract_final_idx(
        dTensor* A, dTensor* B, dTensor* C,
        int n_shared_dims, int permute, int* pi_A, int* pi_B
    ) {
    /*
    Tensor contraction A-B --> C 

    Indexing 

        A[i_1,...,i_n,a_1,...,a_s]        i's unshared, a's shared
        B[j_1,...,j_m,a_1,...,a_s]        j's unshared, a's shared

        C[pi_A(i_1), ..., pi_A(i_n), pi_B(j_1), ..., pi_B(j_m)]
    */
    
    // Number of rows and columns in the "matricized" tensors 
    // A[i,k] and B[j,k]

    // contract_tensors_final_idx_kernel()

}



