#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cuda.h>
#include "dtensor.cuh"
#include "henon_heiles.cuh"
#include "my_cuda_util.cuh"

#define W 32 // Warp size 

__global__ void henon_heiles_contract_right_kernel(
        dTensor* __restrict__ X,         // Input tensor
        dTensor* __restrict__ Y,         // Output tensor
        const int n_a,            // Number of degrees of freedom for mode-1 of X
                                  //    Should match X->shape[0]
        const int n_dof,          // Number of physical degrees of freedom.
                                  //    Should match X->shape[2]
        const float lambda,       // Scaling coefficient for the potential
        const float q_min,        // min/max q-values for the site 
        const float q_max,
        const int N_WpBlk
    ) {
    /* 
        Operator Core   H[gkm1,ikp,gk,ik]

                        ikp
                        |
                  gkm1--H--gk
                        |
                        ik

        Input Tensor  X[a,gk,ik,b]  STORED COLUMN MAJOR

                        a
                        |
                    gk--
                        X
                    ik--
                        |
                        b 

        Output Tensor
            Y[a,ikp,gkm1,b] = sum_{gk,ik} H[gkm1,ikp,gk,ik] X[a,gk,ik,b]

        b fixed within block
        a varies continuously within warp
    */
    
    // Grid and Block Reference
    // blockIdx.x  = Index 'b' for the tensors X and Y
    // threadIdx.x = Thread index within the warp 
    // threadIdx.y = Warp index within the block

    // Unique index of amongst all threads within the same y-block
    // All of these threads have the same b value
    //int b_blk_idx =  (N_WpBlk * W) * blockIdx.y + (threadIdx.x + W * threadIdx.y);
    int b_blk_idx =  (N_WpBlk * W) * blockIdx.y + (W * threadIdx.x + threadIdx.y);


    // Determining elements Y(a,ik,:,b) that this thread will compute
    int b   = blockIdx.x;               // Fixed within block
    int ik  = b_blk_idx / n_a;          // Fixed within warp, can vary within block
    int a   = b_blk_idx % n_a;          // Varies continuously within warp

    // Henon-Heiles specific values
    float h = 1. / (n_dof - 1);                 // Spacing in physical space
    float q_i = q_min + h * (q_max-q_min) * ik ; // Physical variable assoc. with this thread

    // Indexing offsets
    int ab = a + (n_a * 3 * n_dof) * b;
    int x_ik = (n_a * 3) * ik;
    int y_ik = n_a * ik;

    /* gkm1 = gk = 0 --- Identity block */
    // Y(a,ik,0,b) = X(a,0,ik,b)
    double tmpX = (X->t)[ab + x_ik];
    (Y->t)[ab + y_ik] = tmpX;

    /* gkm1=1, gk=0 --- Diagonal block C_k */
    // Y(a,ik,1,b) = C_k[ik,ik] * X(a,0,ik,b)
    (Y->t)[ab + y_ik + n_a*n_dof] = lambda * q_i * tmpX;

    /* gkm1=2, gk=0 --- Tridiagonal block A_k */
    // Main Diagonal A_k[i,i] X[a,0,ik,b]
    double tmpY = (-2./(h*h) - (lambda/3.)*(q_i*q_i*q_i) ) * tmpX;
    // Subdiagonal diagonal A_k[i,i-1] X[a,0,ik-1,b]
    tmpX = (ik > 0) ? (X->t)[ab + 3*n_a*(ik-1)] : 0;
    tmpY += (1./(h*h)) * tmpX;
    // Super-diagonal A_k[i,i+1] X[a,0,ik+1,b]
    tmpX = (ik+1 < n_dof) ? (X->t)[ab + 3*n_a*(ik+1)] : 0;
    tmpY += (1./(h*h)) * tmpX;
    
    /* gkm1=2, gk=1 --- Diagonal block B_k X[a,1,ik,b] */
    tmpY += (q_i*q_i) * (X->t)[ab+x_ik+1*n_a]; // GMEM access

    /* gkm1=2, gk=2 --- Identity block X[a,2,ik,b] */
    tmpY += (X->t)[ab+x_ik+2*n_a]; // GMEM access

    /* Final write for gkm1=2*/
    // Y[a,ik,2,b]
    (Y->t)[ab+y_ik+2*n_a*n_dof] = tmpY;
}


__host__ void henon_heiles_contract_right(
        dTensor* __restrict__ X,
        dTensor* __restrict__ Y,
        const float lambda,
        const float q_min,  
        const float q_max,
        const int N_WpBlk
    ) {
    /*
        Launcher for the above kernel,
        allowing for variable number of warps per block
    */
    int n_a = X->h_shape[0];
    // int n_g = X->shape[1], should equal 3
    int n_dof = X->h_shape[2];
    int n_b = X->h_shape[3];

    /* Determining the grid and block size */
    int B_dim_x = n_b;
    int B_dim_y = (n_a * n_dof + N_WpBlk * W - 1) / (N_WpBlk * W);
    dim3 grid_shape(B_dim_x, B_dim_y);
    //dim3 block_shape(W,N_WpBlk);
    dim3 block_shape(N_WpBlk,W);

    // printf("%d x %d Grid with %d x %d blocks\n", B_dim_x, B_dim_y, W, N_WpBlk);
    /* Launch the kernel */
    henon_heiles_contract_right_kernel
        <<<grid_shape, block_shape>>>(
            X->dT, Y->dT, n_a, n_dof, lambda, q_min, q_max, N_WpBlk
    );

}


__host__ void henon_heiles_contract_right(
        dTensor* __restrict__ X,
        dTensor* __restrict__ Y,
        const float lambda,
        const float q_min,  
        const float q_max
    ) {
    /*
        Launcher for the above kernel 
        Uses a fixed 16 warps per block
    */
    henon_heiles_contract_right(X, Y, lambda, q_min, q_max, 16);
}

