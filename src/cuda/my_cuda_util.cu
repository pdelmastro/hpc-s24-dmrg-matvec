#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <my_cuda_util.cuh>

#define p_B 64

/*
 Source
 https://stackoverflow.com/questions/14038589/
 what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api%5B/url%5D
 */
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

void set_device(int device_num) {
    /*
    Function to specific which device to run on
    */
    gpuErrchk( cudaSetDevice(device_num) );
}

/*
----------------------------------------------------------------------
    Device Primitives
----------------------------------------------------------------------
*/
void* new_device_variable(size_t size) {
    /* 
        Returns a device pointer to a new variable of given size size
     */
    void* arr;
    gpuErrchk( cudaMalloc(&arr, size) );
    return arr;
}

void set_device_variable(void* d_ptr, void* h_ptr, size_t size) {
    /*
        Sets the value of the device variable pointed to be 'ptr'
        to be 'val' 
    */
    gpuErrchk( cudaMemcpy(d_ptr, h_ptr, size, cudaMemcpyHostToDevice) );
}

void* get_device_variable(void* d_ptr, size_t size) {
    /*
        Gets the value of the device variable pointed to be 'ptr'
    */
    void* h_ptr = NULL;
    gpuErrchk( cudaMemcpy(h_ptr, d_ptr, size, cudaMemcpyDeviceToHost) );
    return h_ptr;
}

void free_device_variable(void* v) {
    gpuErrchk( cudaFree(v) );
}


/*
----------------------------------------------------------------------
    Basic Device Array Manipulation
----------------------------------------------------------------------
*/

void* new_device_array(int N, size_t size) {
    /*
     Function for allocating new arrays on the device
     - 'N' specifies the number of elements
     - 'size' specifies the size of each element
     */
    void* arr;
    gpuErrchk( cudaMalloc(&arr, N*size) );
    return arr;
}

void copy_array_to_device(int N, size_t size, void* h_arr, void* d_arr) {
    /*
     Function to copy an array from host to device
     */
    gpuErrchk( cudaMemcpy(d_arr, h_arr, N*size, cudaMemcpyHostToDevice) );
}

void copy_array_from_device(int N, size_t size, void* d_arr, void* h_arr) {
    /*
     Function to copy an array from device to host
     */
    gpuErrchk( cudaMemcpy(h_arr, d_arr, N*size, cudaMemcpyDeviceToHost) );
}

void free_device_array(void* d_arr) {
    /*
    Function to free an array on the device
    */
    gpuErrchk( cudaFree(d_arr) );
}

void zero_device_array(int N, size_t size, void* d_arr) {
    /*
    Sets all of the elements in the array to zero
    */
    gpuErrchk( cudaMemset(d_arr, 0, N*size) );
}


/*
----------------------------------------------------------------------
    Random number generation
----------------------------------------------------------------------
*/

__global__ void new_curand_states_kernel(int N, curand_state* states, int seed) {
    /*
        Initializes an array of random states on the device
    */
    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    if (tid < N)
        curand_init(seed+tid, tid, 0, &states[tid]);
}

curand_state* new_curand_states(int N, int seed) {
    /*
        Returns a new array of N initialized curand_states
    */
   curand_state* states = (curand_state*)new_device_array(N, sizeof(curand_state));
   new_curand_states_kernel<<<(N+p_B-1)/p_B, p_B>>>(N, states, seed);
   return states;
}

__global__ void urand_device_array_kernel(int N, double* d_arr, curand_state* states) {
    /*
        Randomizes the values in the provided double array, with each elements
        being drawn iid from U[-1,1] 
    */
    int i = blockIdx.x*blockDim.x + threadIdx.x; 
    if (i < N)
        d_arr[i] = 2 * curand_uniform_double(&states[i]) - 1;
}

void urand_device_array(int N, double* d_arr, curand_state* states) {
    /*
        Launcher for the kernel above 

        Randomizes the values in the provided double array, with each elements
        being drawn iid from U[-1,1] 
    */
    urand_device_array_kernel<<<(N+p_B-1)/p_B, p_B>>>(N, d_arr, states);
    fflush(stdout);
}







/*
----------------------------------------------------------------------
    Misc
----------------------------------------------------------------------
*/
__device__ int device_int_array_product(int N, int* d_arr) {
    /*
    Function to compute the product of all elements in an 
    integer array. Intended to be called on small arrays, 
    from a only a few threads
    */
    int prod = 1;
    for (int i = 0; i < N; i++)
        prod *= d_arr[i];
    return prod;
}



