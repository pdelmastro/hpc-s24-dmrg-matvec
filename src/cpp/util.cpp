/* Miscellaneous utility functions */

using namespace std;
#include <stdlib.h>
#include <string>
#include <iostream>
#include <util.hpp>


float urandf() {
    /* Returns a random float drawn from U[0,1] */
    return (float)rand()/(float)(RAND_MAX);
}

int sign(double d) {
    /* Returns the sign of a double */
    return (d >= 0) ? 1 : -1;
}

int int_min(int a, int b) {
    /* returns the min of two ints */
    return (a > b) ? a : b;
}

int int_array_contains(int* arr, int length, int query) {
    /* Checks to see if the 'query' is contained
       within the array 'arr'

       Returns the first index of the match, if found
       Otherwise returns -1
     */
    for (int i = 0; i < length; i ++) {
        if (arr[i] == query) { return i; }
    }
    return -1;
}

int str_array_contains(string str, char query) {
    /* Checks to see if the 'query' is contained
       within the array 'arr'

       Returns the first index of the match, if found
       Otherwise returns -1
     */
    for (int i = 0; i < str.length(); i++) {
        if (str[i] == query) { return i; }
    }
    return -1;
}
