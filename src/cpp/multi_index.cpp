#include <malloc.h>
#include "../include/multi_index.hpp"

#include <iostream> 

/*
 -------------------------------------------------------------------------------
  MultiIndex Functions 
 -------------------------------------------------------------------------------
 */

MultiIndex::MultiIndex(int dim, int* shape) {
    this->dim=dim;
    this->shape = (int*)malloc(sizeof(int)*dim);
    for (int i = 0; i < dim; i++) {
        this->shape[i] = shape[i];
    }
    this->curr = (int*)malloc(sizeof(int)*dim);
    this->zero();
}

MultiIndex::~MultiIndex() {
    free(this->shape);
    free(this->curr);
}

void MultiIndex::zero() {
    for (int i = 0; i < this->dim; i++) {
        this->curr[i] = 0;
    }
}

int& MultiIndex::operator[](int idx) {
    return this->curr[idx];
}

int MultiIndex::step() {
    /*
     Increments the MultiIndex by 1, or resets the MultiIndex
     to (0,...,0) if already at the last index 
     (shape[0]-1, ..., shape[-1]-1).
     
     Returns 1 in the former case and 0 in the latter case. 
     */
    for (int i = 0; i < this->dim; i++) {
        // Increment the index if possible
        if (this->curr[i] + 1 < this->shape[i]) {
            (this->curr[i])++;
            return 1;
        }
        // Otherwise, reset this index and continue to 
        // increasing the next index
        else {
            this->curr[i] = 0;
        }
    }
    // Special case: All indices reset
    return 0;
}
