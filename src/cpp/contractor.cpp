#include "dtensor.cuh"
#include "multi_index.hpp"
#include "contractor.hpp"
#include "util.hpp"

#include <malloc.h>
using namespace std;
#include <string>
#include <iostream>



Contractor::Contractor(dTensor* A, dTensor* B, string A_fmt, string B_fmt, string C_fmt) {
    /* 
        Contraction constructor
        Automatically parses the contraction pattern,
        but it does not perform the contraction automatically
     */
    this->A = A;
    this->B = B;
    this->parse_contraction_pattern(A_fmt, B_fmt, C_fmt);
}



Contractor::~Contractor() {
    /* 
        Destructor --- just freeing all of the arrays allowed to help
        with performing the contraction
     */
    free(shared_shape);
    free(As_shared_dims);
    free(As_unshared_dims);
    free(As_unshared_dims_perm);
    free(Bs_shared_dims);
    free(Bs_unshared_dims);
    free(Bs_unshared_dims_perm);
}




void Contractor::parse_contraction_pattern(string A_fmt, string B_fmt, string C_fmt) {
    /*
        Helper function for tensor contractions.
        Used to parse strings like 'ij,jk->ik' used to define the 
        pattern of tensor contractions. 
     */
    int i, j, k;
 
    // Verify the parsed dimensions match up with the tensors A and B
    if ( A_fmt.length() != this->A->dim || B_fmt.length() != this->B->dim ) {
        throw runtime_error("dTensor Contraction"
                            "--- Wrong number of input dimensions\n");
    }

    // Determine the number of shared dimensions between the two tensors
    n_shared_dims = 0;
    // Looping over dimensions of A
    for (i = 0; i < A->dim; i++) {
        if (str_array_contains(B_fmt, A_fmt[i]) != - 1) {
            n_shared_dims++;
        }
    }

    // Verify the number of unshared dimensions matches the number
    // of dimensions of the result of the contraction
    n_As_unshared_dims = A->dim - n_shared_dims;
    n_Bs_unshared_dims = B->dim - n_shared_dims;
    new_dim = A->dim + B->dim - 2 * n_shared_dims;
    if (C_fmt.length() != new_dim ) {
        throw runtime_error("dTensor Contraction"
                            "--- Wrong number of output dimensions\n");
    }

    // Allocating space for the meta-data now that we know the number
    // of contracted indices
    // Shape of the shared index space
    shared_shape = (int*)malloc(sizeof(int)*n_shared_dims);
    // Indices of the shared dimensions
    As_shared_dims = (int*)malloc(sizeof(int)*n_shared_dims);
    Bs_shared_dims = (int*)malloc(sizeof(int)*n_shared_dims);
    // Indices of the unshared dimensions
    As_unshared_dims = (int*)malloc(sizeof(int)*n_As_unshared_dims);
    Bs_unshared_dims = (int*)malloc(sizeof(int)*n_Bs_unshared_dims);
    // Permutations of the unshared dimensions
    As_unshared_dims_perm = (int*)malloc(sizeof(int)*n_As_unshared_dims);
    Bs_unshared_dims_perm = (int*)malloc(sizeof(int)*n_Bs_unshared_dims);
    // Shape of the new tensor
    new_shape = (int*)malloc(sizeof(int)*new_dim);

    // Filling in the the first two sets of meta-data
    int A_shared_idx = 0;
    int B_shared_idx = 0;
    int A_unshared_idx = 0;
    int B_unshared_idx = 0;
    // Processing in the indices of A
    for (i = 0; i < A->dim; i++) {
        j = str_array_contains(B_fmt, A_fmt[i]);
        if (j == -1) { // Unshared index of A
            As_unshared_dims[A_unshared_idx++] = i;
        } else { // Shared index
            As_shared_dims[A_shared_idx] = i;
            Bs_shared_dims[B_shared_idx++] = j;
            shared_shape[A_shared_idx] = A->shape[i];
            A_shared_idx++;
        }
    } 
    // Post-processing --- making sure we didn't miss any unshared
    // indices of B
    for (j = 0; j < B->dim; j++) {
        i = str_array_contains(A_fmt, B_fmt[j]);
        if (i == -1) { // unshared index of B
            Bs_unshared_dims[B_unshared_idx++] = j;
        }
    }

    // Now for the final meta-data 
    // --- index permutations, based on the format of C
    // --- shape of the new tensor
    for (i = 0; i < n_As_unshared_dims; i++) {
        k = str_array_contains(C_fmt, A_fmt[As_unshared_dims[i]]);
        As_unshared_dims_perm[i] = k;
        new_shape[k] = A->shape[As_unshared_dims[i]];
    }
    for (j = 0; j < n_Bs_unshared_dims; j++) {
        k = str_array_contains(C_fmt, B_fmt[Bs_unshared_dims[j]]);
        Bs_unshared_dims_perm[j] = k;
        new_shape[k] = B->shape[Bs_unshared_dims[j]];
    }
}




dTensor* Contractor::contract() {
    /*
        Contraction Algorithm, computing the contraction of this->A
        and this->B using the contraction pattern (written in Einstein
        summation notation, similar to NumPy's 'einsum') provided in
        the contrustor.  
     */

    /* Allocate space for the new tensor*/
    dTensor* C = new dTensor(new_dim, new_shape);

    /* Contraction calculation */
    MultiIndex* c_idx = new MultiIndex(new_dim, new_shape);
    MultiIndex* shared_idx = new MultiIndex(n_shared_dims, shared_shape);
    int* a_idx = (int*)malloc(sizeof(int)*(A->dim));
    int* b_idx = (int*)malloc(sizeof(int)*(B->dim));
    int s;
    double acc;
    do {
        // Reset the accumulator to zero
        acc = 0;

        // Loop over indices being contracted
        shared_idx->zero();
        do {
            // Full (shared & unshared) indices of A and B 
            // Shared
            for (s = 0; s < n_shared_dims; s++) {
                a_idx[As_shared_dims[s]] = shared_idx->curr[s];
                b_idx[Bs_shared_dims[s]] = shared_idx->curr[s];
            }
            // Unshared
            for (s = 0; s < n_As_unshared_dims; s++) {
                a_idx[As_unshared_dims[s]] = c_idx->curr[As_unshared_dims_perm[s]];
            }
            for (s = 0; s < n_Bs_unshared_dims; s++) {
                b_idx[Bs_unshared_dims[s]] = c_idx->curr[Bs_unshared_dims_perm[s]];
            }
            // Contraction update
            acc += (*A)(a_idx) * (*B)(b_idx);
        
        } 
        while (shared_idx->step());

        // Save the result of the computation to the tensor C
        (*C)(c_idx) = acc;

    }
    while (c_idx->step());

    // Freeing variables created here for the contraction
    free(a_idx);   free(b_idx);   
    delete c_idx;  delete shared_idx;

    // Return the contracted tensor
    return C;

}
