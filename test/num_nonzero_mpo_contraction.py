
import numpy as np
import matplotlib.pyplot as plt

N = 10
G = 3
x_min = -1
x_max = 1
lam = 1
mu = 0
std = 0.5

# Q-values matrix
Q = np.linspace(x_min, x_max, num=N) 

# Triagonal matrix
h = 1 # 2 / (N-1)
A = np.zeros((N,N))
# np.fill_diagonal(A,2/(h*h) + Q*Q*(1 - lam/3*Q))
# A[np.arange(N-1),np.arange(1,N)] = -1./h
# A[np.arange(1,N),np.arange(N-1)] = -1./h
A[np.arange(N),np.arange(N)] = mu + std * np.random.randn(N)
A[np.arange(N-1),np.arange(1,N)] = mu + std * np.random.randn(N-1)
A[np.arange(1,N),np.arange(N-1)] = mu + std * np.random.randn(N-1)

# Diagonal matrices
Id = np.eye(N)
B = np.diag(mu + std*np.random.randn(N)) # np.diag(Q*Q)
C = np.diag(mu + std*np.random.randn(N)) # np.diag(Q)

H = np.zeros((G, G, N, N))
H[0,0] = Id
H[1,0] = C 
H[2,0] = A
H[2,1] = B
H[2,2] = Id


# Hrs = np.zeros((N,G,N,G))
# for gk in range(G):
#     for gkp1 in range(G):
#         for ik in range(N):
#             for ikp1 in range(N):
#                 Hrs[ik,gk,ikp1,gkp1] = H[gk,gkp1,ik,ikp1]

# fig, ax = plt.subplots(1,1,figsize=(6,6), dpi=128)
# ax.plot(np.arange(N*G),np.arange(N*G),'-o', color='red', zorder=1)
# ax.imshow((Hrs.reshape(-1,N*G)) != 0, cmap='binary', zorder=-1)
# ax.set_title(r'Sparsity Pattern of $H[j_{k},\gamma_{k-1},i_{k},\gamma_{k}]$', size=16)
# ax.set_xlabel(r'($j_{k},\gamma_{k-1}$)', size=15)
# ax.set_ylabel(r'($i_{k},\gamma_k$)', size=15)
# plt.show()



# Hrs = np.zeros((N,G,G,N))
# for gk in range(G):
#     for gkp1 in range(G):
#         for ik in range(N):
#             for ikp1 in range(N):
#                 Hrs[ik,gk,gkp1,ikp1] = H[gk,gkp1,ik,ikp1]

# fig, ax = plt.subplots(1,1,figsize=(6,6), dpi=128)
# ax.plot(np.arange(N*G),np.arange(N*G),'-o', color='red', zorder=1)
# ax.imshow((Hrs.reshape(-1,N*G)) != 0, cmap='binary', zorder=-1)
# ax.set_title(r'Sparsity Pattern of $H[j_{k},\gamma_{k-1},i_{k},\gamma_{k}]$', size=16)
# ax.set_xlabel(r'($j_{k},\gamma_{k-1}$)', size=15)
# ax.set_ylabel(r'($i_{k},\gamma_k$)', size=15)
# plt.show()



Hrs = np.zeros((G,N,G,N))
for gk in range(G):
    for gkp1 in range(G):
        for ik in range(N):
            for ikp1 in range(N):
                Hrs[gk,ik,gkp1,ikp1] = H[gk,gkp1,ik,ikp1]

fig, ax = plt.subplots(1,1,figsize=(6,6), dpi=128)
ax.imshow(np.abs(Hrs.reshape(-1,N*G)), cmap='binary', zorder=-1)
ax.set_title(r'Abs Val of $H[j_{k},\gamma_{k-1},i_{k},\gamma_{k}]$', size=16)
ax.set_xlabel(r'($\gamma_{k-1},j_{k}$)', size=15)
ax.set_ylabel(r'($\gamma_k,i_{k}$)', size=15)
ax.set_xticks(np.arange(N*G + 1) - .5, minor=True)
ax.set_yticks(np.arange(N*G + 1) - .5, minor=True)
ax.grid(which="minor", color="gray", linewidth=0.5)
ax.tick_params(which="minor", bottom=False, left=False)
plt.show()



# Hrs = np.zeros((G,N,N,G))
# for gk in range(G):
#     for gkp1 in range(G):
#         for ik in range(N):
#             for ikp1 in range(N):
#                 Hrs[gk,ik,ikp1,gkp1] = H[gk,gkp1,ik,ikp1]


# fig, ax = plt.subplots(1,1,figsize=(6,6), dpi=128)
# ax.imshow((Hrs.reshape(-1,N*G)) != 0, cmap='binary', zorder=-1)
# ax.set_title(r'Sparsity Pattern of $H[j_{k},\gamma_{k-1},i_{k},\gamma_{k}]$', size=16)
# ax.set_xlabel(r'($\gamma_{k-1},j_{k}$)', size=15)
# ax.set_ylabel(r'($i_{k},\gamma_k$)', size=15)
# plt.show()


        

        