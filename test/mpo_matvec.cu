/*
 Basic Test --- Application of MPO
*/

#include <cassert>
#include <stdio.h>
#include <string>
#include <cuda.h>
#include <util.hpp>
#include <dtensor.cuh>
#include <contraction.cuh>
#include <henon_heiles.cuh>
#include <my_cuda_util.cuh>

int main(int argc, char* argv[]) {

    // DMRG Parameters
    const int phys_dim = atoi(argv[1]);
    const int bond_dim = atoi(argv[2]);
    const int op_dim = 3;
    const float lambda = 1.0;
    const float q_min = -0.5;  
    const float q_max = 0.5;

    // Test Parameters
    const int N_WpBlk = atoi(argv[3]);
    const int n_tests = atoi(argv[4]);


    /*
    ----------------------------------------------------------
        Setup tensors on the device
    ----------------------------------------------------------
    */
    
    // Merged core 
    int* m_shape = (int*)malloc(sizeof(int)*4);
    m_shape[0] = bond_dim;
    m_shape[1] = phys_dim;  
    m_shape[2] = phys_dim;   
    m_shape[3] = bond_dim; 
    dTensor* d_M = dTensor::new_device_dtensor(4, m_shape);

    // Environment
    int* env_shape = (int*)malloc(sizeof(int)*3);
    env_shape[0] = bond_dim;
    env_shape[1] = op_dim;  
    env_shape[2] = bond_dim;   
    dTensor* d_E = dTensor::new_device_dtensor(3, env_shape);

    // Intermediate 1
    int* t1_shape = (int*)malloc(sizeof(int)*4);
    t1_shape[0] = bond_dim;
    t1_shape[1] = op_dim;  
    t1_shape[2] = phys_dim;
    t1_shape[3] = phys_dim * bond_dim;
    dTensor* d_T1 = dTensor::new_device_dtensor(4, t1_shape);

    // Intermediate 2
    int* t2_init_shape = (int*)malloc(sizeof(int)*4);
    t2_init_shape[0] = bond_dim;
    t2_init_shape[1] = phys_dim;  
    t2_init_shape[2] = op_dim;
    t2_init_shape[3] = bond_dim * phys_dim;
    dTensor* d_T2 = dTensor::new_device_dtensor(4, t2_init_shape);
    int* t2_re_shape = (int*)malloc(sizeof(int)*4);
    t2_re_shape[0] = bond_dim * phys_dim;
    t2_re_shape[1] = op_dim;  
    t2_re_shape[2] = phys_dim;
    t2_re_shape[3] = bond_dim;
    
    // Intermediate 3
    int* t3_init_shape = (int*)malloc(sizeof(int)*4);
    t3_init_shape[0] = bond_dim * phys_dim;
    t3_init_shape[1] = phys_dim;  
    t3_init_shape[2] = op_dim;
    t3_init_shape[3] = bond_dim;
    dTensor* d_T3 = dTensor::new_device_dtensor(4, t3_init_shape);

    printf("Tensors creates on device\n");
    fflush(stdout);

    /*
    ----------------------------------------------------------
        Profiling Experiment
    ----------------------------------------------------------
    */


    for (int i = 0; i < n_tests; i++) {
     
        /* Step 1: Contract Merged Core and Environment */
        //contract_dtensors_gemm(d_E, d_M, d_T1, 1);
        //printf("  GEMM contraction 1 complete\n");
        //fflush(stdout);

        /* Step 2: Intermediate 1 with Operator Core */
        henon_heiles_contract_right(d_T1, d_T2, lambda, q_min, q_max, N_WpBlk);
        // Reshape
        d_T2->reshape_fixed_dim(t2_re_shape);

        /* Step 3: Intermediate 2 with Operator Core */
        henon_heiles_contract_right(d_T2, d_T3, lambda, q_min, q_max, N_WpBlk);
        // Reshape
        d_T2->reshape_fixed_dim(t2_init_shape);
        
        /* Step 4: Intermediate 3 with Left Environment*/
        // contract_dtensors_gemm(d_T2, d_E, d_T1, 1);
        //contract_dtensors_gemm(d_E, d_T3, d_M, 2);
        //printf("  GEMM contraction 2 complete\n");
        //fflush(stdout);
    }
    
    
    /* Clean up */
    printf("Cleaning up ...\n");
    fflush(stdout);
    delete d_M;
    delete d_E;
    delete d_T1;
    delete d_T2;
    delete d_T3;
    printf("Done!\n");
    fflush(stdout);
}