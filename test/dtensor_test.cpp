/*
 Basic tests of the d3Tensor functionality
 */

#include <cassert>
#include <stdio.h>
#include <string>
#include "../include/dtensor.cuh"

int main() {

    /* Creating a zero d3Tensor */
    int dim = 3;
    int d0 = 2; int d1 = 3; int d2 = 5;
    dTensor* T = new dTensor(dim, d0, d1, d2);
    T->zero();
    printf("Created zero-tensor with %d elements\n", (*T).numel);

    /* Testing that all of the elements are zeros */
    int i0, i1, i2;
    for (i0 = 0; i0 < d0; i0++) {
        for(i1 = 0; i1 < d1; i1++) {
            for(i2 = 0; i2 < d2; i2++) {
                assert((*T)(i0,i1,i2) == 0.0);
            }      
        }
    }
    printf("Passed zeros test\n");
    
    /* Setting tensor elements */
    for (i0 = 0; i0 < d0; i0++) {
        for(i1 = 0; i1 < d1; i1++) {
            for(i2 = 0; i2 < d2; i2++) {
                (*T)(i0,i1,i2) = i2 + d1*i1 + d1*d2*i0;
            }      
        }
    }
    // Testing the changes
    int expected;
    for (i0 = 0; i0 < d0; i0++) {
        for(i1 = 0; i1 < d1; i1++) {
            for(i2 = 0; i2 < d2; i2++) {
                expected = i2 + d1*i1 + d1*d2*i0;
                assert((*T)(i0,i1,i2) == expected);
            }      
        }
    }
    printf("Passed element updates test\n");

    /* Deleting the tensor*/
    delete T;
}
