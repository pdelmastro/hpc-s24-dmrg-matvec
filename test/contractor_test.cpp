/*
 Basic tests of the d3Tensor functionality
 */

using namespace std;
#include <cassert>
#include <stdio.h>
#include <string>
#include <iostream>
#include "../include/dtensor.cuh"
#include "../include/multi_index.hpp"
#include "../include/util.hpp"

int main() {

    dTensor *T, *C, *V, *U;
    MultiIndex *t_idx, *v_idx, *c_idx, *u_idx;
    double expected, rel_err;

    /* Creating a zero dTensor */
    int T_dim = 3;
    int T_d0 = 4; int T_d1 = 5; int T_d2 = 3;
    T = new dTensor(T_dim, T_d0, T_d1, T_d2);
    T->zero();
   
    /* Contraction test 1 --- contracting a zero tensor with itself */
    C = dTensor::contract(T, T, "ijk", "ijl", "kl");
    assert(C->dim == 2);
    assert(C->shape[0] == T_d2);
    assert(C->shape[1] == T_d2);
    for (int i = 0; i < T_d2; i++) {
        for (int j = 0; j < T_d2; j++) {
            assert( (*C)(i,j) == 0.0 );
        }
    }
    delete C;
    cout << "Passed Test 1 (zeros)\n";


    /* Contraction test 2 --- contracting a ones tensor with itself */
    T->zero();
    t_idx = new MultiIndex(T->dim, T->shape);
    for(int i = 0; i < T->numel; i++) {
        (*T)(t_idx) = 1.0;
        t_idx->step();
    }
    C = dTensor::contract(T, T, "ijk", "ijl", "kl");
    assert(C->dim == 2);
    assert(C->shape[0] == T_d2);
    assert(C->shape[1] == T_d2);
    for (int i = 0; i < T_d2; i++) {
        for (int j = 0; j < T_d2; j++) {
            assert( (*C)(i,j) == T_d0*T_d1 );
        }
    }
    delete C;
    cout << "Passed Test 2 (ones)\n";


    /* Contraction test 3 --- contracting two non-trivial tensors */
    // Creating a new, larger tensor
    int V_dim = 5;
    int V_d0 = T_d0;  int V_d1 = T_d1;  int V_d2 = T_d2; int V_d3 = 6; int V_d4 = 2;
    V = new dTensor(V_dim, V_d0, V_d1, V_d2, V_d3, V_d4);
    V->zero();
    // Filling V with some non-trivial values
    v_idx = new MultiIndex(V->dim, V->shape);
    for(int i = 0; i < V->numel; i++) {
        (*V)(v_idx) = i;
        v_idx->step();
    }
    // Perform the contraction
    C = dTensor::contract(T, V, "ijk", "ijklm", "lm");
    // Verification
    c_idx = new MultiIndex(C->dim, C->shape);
    for(int i = 0; i < C->numel; i++) {
        // Filling in the unshared indices
        (*v_idx)[3] = (*c_idx)[0];
        (*v_idx)[4] = (*c_idx)[1];
        // Compute the expected value for this index
        expected = 0.0;
        t_idx->zero();
        for (int s = 0; s < T->numel; s++) {
            (*v_idx)[0] = (*t_idx)[0]; 
            (*v_idx)[1] = (*t_idx)[1]; // Shared indices
            (*v_idx)[2] = (*t_idx)[2];
            expected += (*T)(t_idx) * (*V)(v_idx);
            t_idx->step();
        }
        // Assertion
        assert((*C)(c_idx) == expected);
        // Step to the next index
        c_idx->step();
    }
    delete C;
    delete c_idx;
    cout << "Passed Test 3 (ones and non-zeros)\n";


    /* Contraction test 4 --- same as above, just permuting indices */
    // Perform the contraction
    C = dTensor::contract(T, V, "ijk", "ijklm", "ml");
    // Verification
    c_idx = new MultiIndex(C->dim, C->shape);
    for(int i = 0; i < C->numel; i++) {
        // Filling in the unshared indices
        (*v_idx)[3] = (*c_idx)[1];
        (*v_idx)[4] = (*c_idx)[0];
        // Compute the expected value for this index
        expected = 0.0;
        t_idx->zero();
        for (int s = 0; s < T->numel; s++) {
            (*v_idx)[0] = (*t_idx)[0];
            (*v_idx)[1] = (*t_idx)[1];
            (*v_idx)[2] = (*t_idx)[2];
            expected += (*T)(t_idx) * (*V)(v_idx);
            t_idx->step();
        }
        // Assertion
        assert((*C)(c_idx) == expected);
        // Step to the next index
        c_idx->step();
    }
    delete C;
    delete c_idx;
    cout << "Passed Test 4 (ones and non-zeros permuted)\n";



    /* Contraction test 5 --- contracting two non-trivial tensors */
    // Creating a new, larger tensor
    int U_dim = 5;
    int U_d0 = 3;  int U_d1 = 6;  int U_d2 = V_d2; int U_d3 = V_d3; int U_d4 = V_d4;
    U = new dTensor(U_dim, U_d0, U_d1, U_d2, U_d3, U_d4);
    // Filling V with some non-trivial values
    u_idx = new MultiIndex(U->dim, U->shape);
    for(int i = 0; i < U->numel; i++) {
        (*U)(u_idx) = urandf();
        u_idx->step();
    }
    // Perform the contraction
    C = dTensor::contract(U, V, "ijklm", "xyklm", "xiyj");
    // Verification
    c_idx = new MultiIndex(C->dim, C->shape);
    for(int i = 0; i < C->numel; i++) {
        // Filling in the unshared indices
        (*u_idx)[0] = (*c_idx)[1];
        (*u_idx)[1] = (*c_idx)[3];
        (*v_idx)[0] = (*c_idx)[0];
        (*v_idx)[1] = (*c_idx)[2];
        // Compute the expected value for this index
        expected = 0.0;
        for(int i2 = 0; i2 < V_d2; i2++) {          // Looping over shared indices
            (*v_idx)[2] = i2;
            (*u_idx)[2] = i2;
            for (int i3 = 0; i3 < V_d3; i3++) {
                (*v_idx)[3] = i3;
                (*u_idx)[3] = i3;
                for (int i4 = 0; i4 < V_d4; i4++) {
                    (*v_idx)[4] = i4;
                    (*u_idx)[4] = i4;
                    expected += (*U)(u_idx) * (*V)(v_idx);
                }
            }
        }
        // Assertion
        assert((*C)(c_idx) == expected);
        // Step to the next index
        c_idx->step();
    }
    delete C;
    delete c_idx;
    cout << "Passed Test 5 (non-zeros and non-zeros)\n";
    

    /* Deleting the tensor*/
    delete T;  delete V;  delete U;
    delete t_idx,  delete v_idx;   delete u_idx;
}